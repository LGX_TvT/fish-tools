package com.lgx.fishtools.notebook.service;

import com.lgx.fishtools.notebook.model.*;

import java.util.List;

public interface NoteBookSpider {

    /**
     * 根据名称查找小说
     * @param title 搜搜
     * @return Result<List<Book>>
     */
    Result<List<Book>> getBooks(String title);

    /**
     * 根据URL获取具体小说信息
     * @param url URL
     * @return Result<BookInfo>
     */
    Result<BookInfo> getBookInfo(String url);

    /**
     * 根据URL获取具体章节内容
     * @param url URL
     * @return Result<String>
     */
    Result<String> getContent(String url);

}
