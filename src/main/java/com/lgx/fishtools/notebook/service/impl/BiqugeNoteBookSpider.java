package com.lgx.fishtools.notebook.service.impl;

import com.lgx.fishtools.notebook.model.Book;
import com.lgx.fishtools.notebook.model.BookInfo;
import com.lgx.fishtools.notebook.model.Chapter;
import com.lgx.fishtools.notebook.model.Result;
import com.lgx.fishtools.notebook.service.NoteBookSpider;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * 笔趣阁小说爬虫
 * https://www.xxxbiquge.com/
 */
public class BiqugeNoteBookSpider implements NoteBookSpider {

    @Override
    public Result<List<Book>> getBooks(String title) {
        List<Book> bookList = new ArrayList<>();
        try {
            Connection connect = Jsoup.connect("https://www.xxxbiquge.com/index.php?s=/web/index/search");
            connect.data("name", title);
            Document document = connect.post();
            Elements baseBookElement = document.select("div.novelslist2 > ul > li");
            for (Element element : baseBookElement) {
                if (element == baseBookElement.get(0)) continue;
                Document parse = Jsoup.parse(element.html());
                Element bookElement = parse.select("span.s2.wid > a").get(0);
                Element authorElement = parse.select("span.s4.wid > a").get(0);
                Element newChaptorElement = parse.select("span.s3.wid3 > a").get(0);
                Element dateElement = parse.select("span.s6.wid6").get(0);

                String bookName = bookElement.text();
                String bookUrl = "https://www.xxxbiquge.com" + bookElement.attr("href");
                String bookAuthor = authorElement.text();
                String newChaptor = newChaptorElement.text();
                String date = dateElement.text();
                String status = "暂无";

                Book book = new Book().setUrl(bookUrl)
                        .setBookName(bookName)
                        .setBookAuthor(bookAuthor)
                        .setDate(date)
                        .setNewChapter(newChaptor)
                        .setStatus(status);
                bookList.add(book);
            }
            return Result.buildSuccess(bookList);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.errorSuccess("查找失败！！！");
        }
    }

    @Override
    public Result<BookInfo> getBookInfo(String url) {
        try {
            BookInfo bookInfo = new BookInfo();
            List<Chapter> chapters = new ArrayList<>();

            Document document = Jsoup.connect(url).get();

            Element bookTitleElement = document.select("#info > h1").get(0);
            Element authorElement = document.select("#info > p:nth-child(2) > a").get(0);
            Element introductionElement = document.select("#intro").get(0);

            String bookName = bookTitleElement.text();
            String bookAuthor = authorElement.text();
            String introduction = introductionElement.text();

            Elements chaptersElements = document.select("#list > dl > dd > a");
            for (Element chaptersElement : chaptersElements) {
                String chapterName = chaptersElement.text();
                String chapterUrl = "https://www.xxxbiquge.com" + chaptersElement.attr("href");
                Chapter chapter = new Chapter().setTitle(chapterName).setUrl(chapterUrl);
                chapters.add(chapter);
            }

            bookInfo.setBookUrl(url)
                    .setBookName(bookName)
                    .setBookAuthor(bookAuthor)
                    .setIntroduction(introduction)
                    .setChapters(chapters);

            return Result.buildSuccess(bookInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.errorSuccess("获取基本信息失败！！！");
        }
    }

    @Override
    public Result<String> getContent(String url) {
        try {
            Document document = Jsoup.connect(url).get();
            Element element = document.select("#content").get(0);
            String content = element.html()
                    .replace("<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>","")
                    .replace("<br>", "\n")
                    .replace("&nbsp;", " ")
                    .replace("\n\n", "\n");
            return Result.buildSuccess(content);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.errorSuccess("获取文章内容失败！！！");
        }
    }


}
