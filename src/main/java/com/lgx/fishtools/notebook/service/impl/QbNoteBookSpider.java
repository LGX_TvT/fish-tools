package com.lgx.fishtools.notebook.service.impl;

import com.lgx.fishtools.notebook.model.BookInfo;
import com.lgx.fishtools.notebook.service.NoteBookSpider;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * 全本小说爬虫
 */
public class QbNoteBookSpider {


    public static void main(String[] args) {
        //获取Jsoup访问url链接的文档对象
        try {
            String urlStr = "https://www.qb5.la/book_44996/";
            URL url = new URL(urlStr);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection(); //默认就是Get，可以采用post，大小写都行，因为源码里都toUpperCase了。 connection.setRequestMethod("GET"); //是否允许缓存，默认true。 connection.setUseCaches(Boolean.FALSE); //是否开启输出输入，如果是post使用true。默认是false //connection.setDoOutput(Boolean.TRUE); //connection.setDoInput(Boolean.TRUE); //设置请求头信息 connection.addRequestProperty("Connection", "close"); //设置连接主机超时（单位：毫秒）
            connection.setConnectTimeout(8000);
            //设置从主机读取数据超时（单位：毫秒）
            connection.setReadTimeout(8000);

            Document document = Jsoup.parse(connection.getInputStream(), "GBK", urlStr);

            System.out.println(new String(document.text().getBytes(StandardCharsets.UTF_8)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
