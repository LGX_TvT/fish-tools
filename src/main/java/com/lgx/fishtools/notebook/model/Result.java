package com.lgx.fishtools.notebook.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Result<T> {

    private Boolean success;

    private String code;

    private String message;

    private T data;

    public static <T> Result<T> buildSuccess(T data) {
        return new Result<T>().setSuccess(true).setMessage("成功").setCode("20000").setData(data);
    }

    public static <T> Result<T> errorSuccess(String message) {
        return new Result<T>().setSuccess(false).setMessage(message).setCode("40000");
    }

    public static <T> Result<T> errorSuccess() {
        return new Result<T>().setSuccess(false).setMessage("失败").setCode("40000");
    }
}
