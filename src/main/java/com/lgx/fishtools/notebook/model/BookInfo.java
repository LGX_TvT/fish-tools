package com.lgx.fishtools.notebook.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class BookInfo {

    private String bookUrl;

    private String bookName;

    private String bookAuthor;

    private String introduction;

    private List<Chapter> chapters;

}
