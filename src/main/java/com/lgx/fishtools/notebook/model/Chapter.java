package com.lgx.fishtools.notebook.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Chapter {

    private String title;

    private String url;

    @Override
    public String toString() {
        return title;
    }
}
