package com.lgx.fishtools.notebook.model;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class Book {

    private String bookName;

    private String bookAuthor;

    private String url;

    private String newChapter;

    private String date;

    private String status;

    @Override
    public String toString() {
        return bookName;
    }
}
