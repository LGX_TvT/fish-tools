package com.lgx.fishtools.notebook.ui;

import com.intellij.notification.*;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.wm.ToolWindow;
import com.lgx.fishtools.notebook.model.Book;
import com.lgx.fishtools.notebook.model.BookInfo;
import com.lgx.fishtools.notebook.model.Chapter;
import com.lgx.fishtools.notebook.model.Result;
import com.lgx.fishtools.notebook.service.NoteBookSpider;
import com.lgx.fishtools.notebook.service.impl.BiqugeNoteBookSpider;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Vector;

public class NoteBookToolWindow {
    private JTextField searchTextField;
    private JButton searchBtn;
    private JButton resetBtn;
    private JLabel searchLabel;
    private JTabbedPane tabPane;
    private JPanel infoPanel;
    private JPanel detailPanel;
    private JScrollPane detailScrollPane;
    private JPanel noteBookToolWindowContent;
    private JTable chapterTable;
    private JTextArea chapterContentPane;
    private JTextArea introductionTextArea;
    private JLabel bookNameLabel;
    private JLabel bookAuthorLabel;
    private JPanel basicPanel;
    private JPanel searchResultPanel;
    private JTable searchResultTable;
    private JScrollPane searchResultScrollPane;

    // chapter table
    private static final String[] CHAPTER_COLUMN_NAME = {"章节"};
    private static DefaultTableModel CHAPTER_TABLE_MODEL = new DefaultTableModel(null, CHAPTER_COLUMN_NAME) {
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    // search result table
    private static final String[] BOOK_COLUMN_NAME = {"序号", "书名", "作者", "最新章节", "更新时间", "状态"};
    private static DefaultTableModel BOOK_TABLE_MODEL = new DefaultTableModel(null, BOOK_COLUMN_NAME) {
        public boolean isCellEditable(int row, int column) {
            return false;
        }
    };

    private NoteBookSpider bookSpider = new BiqugeNoteBookSpider();

    public NoteBookToolWindow(ToolWindow toolWindow) {
        init();
        addAction();
    }

    private void init () {
        introductionTextArea.setLineWrap(true);                 //激活自动换行功能
        introductionTextArea.setWrapStyleWord(true);            // 激活断行不断字功能

        chapterTable.setModel(CHAPTER_TABLE_MODEL);
        chapterTable.setEnabled(true);

        searchResultTable.setModel(BOOK_TABLE_MODEL);
        searchResultTable.setEnabled(true);

        chapterContentPane.setLineWrap(true);                 //激活自动换行功能
        chapterContentPane.setWrapStyleWord(true);            // 激活断行不断字功能
    }

    private void addAction() {
        // 点击搜索
        searchBtn.addActionListener(e -> {
            searchBtnClickAction(e);
        });

        // 点击重置
        resetBtn.addActionListener(e -> {
            resetBtnClickAction(e);
        });

        // 查找表格 searchResultTable
        searchResultTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                searchResultTableRowClickAction(e);
            }
        });

        // 点击章节列表
        chapterTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                chapterTableRowClickAction(e);
            }
        });
    }

    private void searchBtnClickAction (ActionEvent e) {
        String title = searchTextField.getText();
        if (title == null || "".equals(title.trim())) {

            return;
        }
        notification("小说查找中，关键字：" + title + " ...", NotificationType.INFORMATION);

        Result<List<Book>> result = bookSpider.getBooks(title);
        if (!result.getSuccess()) {
            Messages.showErrorDialog(result.getMessage(), "提示");
            return;
        }
        List<Book> books = result.getData();
        BOOK_TABLE_MODEL = new DefaultTableModel(null, BOOK_COLUMN_NAME) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        for (int i = 0; i < books.size(); i++) {
            Book book = books.get(i);
            Vector<Object> tableData = new Vector();
            tableData.add((i + 1) + "");
            tableData.add(book);
            tableData.add(book.getBookAuthor());
            tableData.add(book.getNewChapter());
            tableData.add(book.getDate());
            tableData.add(book.getStatus());
            BOOK_TABLE_MODEL.addRow(tableData);
        }
        searchResultTable.setModel(BOOK_TABLE_MODEL);

        notification("小说查找成功 ...", NotificationType.INFORMATION);
    }

    private void resetBtnClickAction(ActionEvent e) {
        searchTextField.setText("");
        BOOK_TABLE_MODEL = new DefaultTableModel(null, BOOK_COLUMN_NAME) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        searchResultTable.setModel(BOOK_TABLE_MODEL);
    }

    private void searchResultTableRowClickAction(MouseEvent e) {
        if (e.getClickCount() == 2) {


            int selectedRow = searchResultTable.getSelectedRow();
            Book book = (Book) searchResultTable.getValueAt(selectedRow, 1);
            notification("正在获取【" + book.getBookName() + "】基础信息 ...", NotificationType.INFORMATION);

            Result<BookInfo> result = bookSpider.getBookInfo(book.getUrl());
            if (!result.getSuccess()) {
                Messages.showErrorDialog(result.getMessage(), "提示");
                return;
            }
            BookInfo bookInfo = result.getData();

            // 赋值
            bookNameLabel.setText(bookInfo.getBookName());
            bookAuthorLabel.setText(bookInfo.getBookAuthor());
            introductionTextArea.setText(bookInfo.getIntroduction());
            introductionTextArea.setCaretPosition(0);
            CHAPTER_TABLE_MODEL = new DefaultTableModel(null, CHAPTER_COLUMN_NAME) {
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };
            for (Chapter chapter : bookInfo.getChapters()) {
                Vector<Object> vector = new Vector<>();
                vector.add(chapter);
                CHAPTER_TABLE_MODEL.addRow(vector);
            }
            chapterTable.setModel(CHAPTER_TABLE_MODEL);
            notification("获取【" + book.getBookName() + "】基础信息成功 ...", NotificationType.INFORMATION);
            tabPane.setSelectedIndex(1);
        }
    }

    private void chapterTableRowClickAction(MouseEvent e) {
        if (e.getClickCount() == 2) {

            int selectedRow = chapterTable.getSelectedRow();
            Chapter chapter = (Chapter) chapterTable.getValueAt(selectedRow, 0);
            notification("正在获取【" + chapter.getTitle() + "】内容 ...", NotificationType.INFORMATION);

            Result<String> result = bookSpider.getContent(chapter.getUrl());
            if (!result.getSuccess()) {
                Messages.showErrorDialog(result.getMessage(), "提示");
                return;
            }
            String content = result.getData();
            chapterContentPane.setText(content);
            chapterContentPane.setCaretPosition(0);
            notification("获取【" + chapter.getTitle() + "】内容成功 ...", NotificationType.INFORMATION);
            tabPane.setSelectedIndex(2);
        }
    }

    public void notification (String msg, NotificationType notificationType) {
        NotificationGroup notificationGroup = new NotificationGroup("fish notebook info", NotificationDisplayType.BALLOON, true);
        Notification notification = notificationGroup.createNotification(msg, notificationType);
        Notifications.Bus.notify(notification);
    }

    public JComponent getContent() {
        return this.noteBookToolWindowContent;
    }
}
